﻿using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace chattyrecode1
{
    class JsonStuff
    {
        static JObject WriteJOBJ;

        public static void ConfigSave(string savedHash, string savedIP)
        {
            WriteJOBJ = new JObject(
                new JProperty("lastConfigChange", DateTime.Now.ToString("yyyy-MM-dd")),
                new JProperty("hash", savedHash),
                new JProperty("savedIP", savedIP)
            );

            try
            {
                File.WriteAllText("config.json", WriteJOBJ.ToString());
            }
            catch (Exception ex)
            {
                DebugPrint.Print(string.Format("Caught this exeption when attempting write to json: {0}!", ex), true);
            }
        }

        public static void LoadJson()
        {
            using (StreamReader file = File.OpenText("config.json"))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                JObject ReadJBOJ = (JObject)JToken.ReadFrom(reader);
                DebugPrint.NoFormat(ReadJBOJ.ToString());
            }
        }
    }
}