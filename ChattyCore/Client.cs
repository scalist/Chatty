using System;
using Lidgren.Network;

namespace chattyrecode1
{
    class Client
	{
		NetClient tclient;
		public bool connected;
		private string code;
		//private bool codeinit; // never used so disabling
		internal bool hasconnnected;

		public Client(string name)
		{
			NetPeerConfiguration config = new NetPeerConfiguration("1ee3a2a5-1c4c-4be2-9848-7d94377c2765");
			//config.Port = 3232;
			config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
			config.EnableMessageType(NetIncomingMessageType.DiscoveryResponse);
			//codeinit = false;
			tclient = new NetClient(config);
			tclient.Start();
		}

		public void Update()
		{
			NetIncomingMessage msg;
			if ((msg = tclient.ReadMessage()) != null)
			{
				switch (msg.MessageType)
				{
					case NetIncomingMessageType.WarningMessage:
						DebugPrint.Print(msg.ReadString(), false);
						break;
					case NetIncomingMessageType.ErrorMessage:
                        DebugPrint.Print(msg.ReadString(), true);
						break;
					case NetIncomingMessageType.StatusChanged:
						NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                        DebugPrint.NoFormat(status.ToString());
						switch (status)
						{
							case NetConnectionStatus.Connected:
                                DebugPrint.Print("Connected!");
								connected = true;
								break;
							case NetConnectionStatus.Disconnecting:
                                DebugPrint.Print(string.Format("You are being disconnected because {0}!", msg.ReadString()), false);
								break;
							case NetConnectionStatus.Disconnected:
                                DebugPrint.Print("You have been disconnected from the server.", false);
								break;
							case NetConnectionStatus.RespondedConnect:
                                DebugPrint.Print("Connecting to server..");
								break;
							case NetConnectionStatus.InitiatedConnect:
                                DebugPrint.Print("Started connecting to server..");
								break;
									default:
                                DebugPrint.Print("Scream at the dev to finish his program!!", false);
								break;
						}
						break;
					case NetIncomingMessageType.Data:
						DataMessage(msg);
						break; 

					default:
                        DebugPrint.NoFormat("Unhandled type: " + msg.MessageType);
						break;
				}
			}
		}

		internal void Sendmsg(string v,DataType d)
		{
			NetOutgoingMessage outmsg = tclient.CreateMessage(v);
			switch (d)
			{
				case DataType.DATATYPEMSG:
					outmsg.Write((byte)DataType.DATATYPEMSG);
					tclient.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
					break;
				case DataType.DATATYPEOTHER:
                    DebugPrint.Print("Attempted to send a datatype other message when sending!", true);
					break;
				case DataType.DATATYPEREQUEST:
					outmsg.Write((byte)DataType.DATATYPEMSG);
					tclient.SendMessage(outmsg, NetDeliveryMethod.ReliableUnordered);
					break;
			}
			
		}

		private void DataMessage(NetIncomingMessage msg)
		{
			NetOutgoingMessage outmsg = tclient.CreateMessage();
			string s = msg.ReadString();
			DataType data = (DataType)msg.ReadByte();

			switch (data)
			{
				case DataType.DATATYPEMSG:
                    DebugPrint.NoFormat(s);
					break;
				case DataType.DATATYPEOTHER:
                    DebugPrint.Print("Attempted to send a datatype other message when reciving!", true);
                    break;
				case DataType.DATATYPEREQUEST:
					if (s.ToLower().StartsWith("getcode"))
					{
						code = s.ToLower().Split(' ')[1];
					}
					break;
				default:
                    DebugPrint.Print("Serverhost sent incorrect message. please make sure the host is verified", false);
					break;
			}
		}

		public int ConnectIp(string ip)
		{
            try
            {
                tclient.Connect(ip, 3232);
                System.Threading.Tasks.Task.Run(() => {
                    System.Threading.Thread.Sleep(5000);
                    hasconnnected = true;
                });

                return tclient.ConnectionsCount;
            }
            catch(Exception ex)
            {
                DebugPrint.Print(string.Format("Could not connect to server correctly! You may have no internet or have connected to a unknown host. Error message: {0}", ex), true);

                Console.ReadKey();
                Environment.Exit(0);

                return 0;
            }
		}
	}
}
