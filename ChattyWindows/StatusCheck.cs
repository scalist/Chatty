﻿using System;
using Lidgren.Network;

namespace chattyrecode1
{
    internal class Serverhost
	{
		private string code;
		NetServer server;
		public Serverhost(string name)
		{
			code = GenCode();
			DebugPrint.Print("encryption key is: " + code);
			NetPeerConfiguration serverconfig = new NetPeerConfiguration("1ee3a2a5-1c4c-4be2-9848-7d94377c2765");
			serverconfig.Port = 3232;
			serverconfig.EnableMessageType(NetIncomingMessageType.DiscoveryResponse);
			serverconfig.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);

			server = new NetServer(serverconfig);
			server.Start();
		}

		private string GenCode()
		{
			Guid g = Guid.NewGuid();
			string GuidString = Convert.ToBase64String(g.ToByteArray());
			GuidString = GuidString.Replace("=", "");
			GuidString = GuidString.Replace("+", "");
			return GuidString;
		}

		public void Update()
		{
			NetIncomingMessage msg;
			if ((msg = server.ReadMessage()) != null)
			{
				switch (msg.MessageType)
				{
					case NetIncomingMessageType.DebugMessage:
					case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.WarningMessage:
                        DebugPrint.Print(msg.ReadString(), false);
						break;
					case NetIncomingMessageType.ErrorMessage:
                        DebugPrint.Print(msg.ReadString(), true);
						break;
					case NetIncomingMessageType.StatusChanged:
						NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
						switch (status)
						{
							case NetConnectionStatus.Connected:
                                DebugPrint.Print("Client has connected!");
								NetOutgoingMessage outmsg = server.CreateMessage("code " + code);
								DebugPrint.Print("code " + code);
								outmsg.Write((byte)DataType.DATATYPEREQUEST);
								server.SendMessage(outmsg,msg.SenderConnection,NetDeliveryMethod.ReliableUnordered);
								break;
							case NetConnectionStatus.InitiatedConnect:
                                DebugPrint.Print("Client is connecting!");
								break;
							case NetConnectionStatus.ReceivedInitiation:
                                DebugPrint.Print("Client is connecting!");
								break;
							case NetConnectionStatus.Disconnecting:
                                DebugPrint.Print("Client is disconnecting!");
								break;
							case NetConnectionStatus.Disconnected:
                                DebugPrint.Print("Client has disconnected because of: " + msg.ReadString());
								break;
							default:
                                DebugPrint.Print("Tell the dev to finish his program", false);
								break;
						}
						break;
					case NetIncomingMessageType.Data:
						DataMessage(msg);
						break;
					default:
                        DebugPrint.Print(string.Format("Unhandled type: {0}!", msg.MessageType), true);
						break;
				}
				server.Recycle(msg);
			}
		}

        private void DataMessage(NetIncomingMessage msg)
		{
			NetOutgoingMessage outmsg = server.CreateMessage();
			msg.Decrypt(new NetAESEncryption(server, code));
			string s = msg.ReadString();
			DataType data = (DataType)msg.ReadByte();
			switch (data)
			{
				case DataType.DATATYPEMSG:
                    DebugPrint.NoFormat(s);
					outmsg.Write(s);
					outmsg.Write((byte)DataType.DATATYPEMSG);
					outmsg.Encrypt(new NetAESEncryption(server, code));
					server.SendToAll(outmsg, NetDeliveryMethod.ReliableUnordered);
					break;
				case DataType.DATATYPEOTHER:
					outmsg.Write((byte)DataType.DATATYPEMSG);
                    DebugPrint.Print("Please try this again later.", false);
					server.SendMessage(outmsg,msg.SenderConnection,NetDeliveryMethod.ReliableUnordered);
					break;
				case DataType.DATATYPEREQUEST:
					break;
				default: msg.SenderConnection.Disconnect("If you believe you where wrongly picked up for editing the code please contact the developer.");
					break;
			}
		}
	}
}
