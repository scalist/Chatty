/*****************************************************************************************************************************************************
Copyright 2018 Zachary Petti

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License
 *****************************************************************************************************************************************************/

using System;
using System.Threading;

namespace chattyrecode1
{
    enum DataType
    {
        DATATYPEMSG = 0,
        DATATYPEREQUEST = 1,
        DATATYPEOTHER = 3
    }

    class Startup
    {
        static bool running;
        static private Serverhost server;
        static private Client client;
        static string hash;

        static void Main(string[] args)
        {
            DebugPrint.Print("Welcome to ChattyWindows, the stable and Windows only Chatty!");

            DebugPrint.Print("Please enter a hash..");
            hash = Console.ReadLine();
			DebugPrint.Print("please type host [name] to host a server and join [ip] to join a server");
            CommandListen();
        }

        private static void CommandListen()
        {
            Console.ForegroundColor = ConsoleColor.Green; // do this because first command will not be green otherwise

            while (true)
            {
                string input = Console.ReadLine();

                if (input.ToLower().StartsWith("help"))
                    DebugPrint.Print("Command page: https://gitlab.com/scalist/Chatty/wikis/commands");
                else if (input.ToLower().StartsWith("host"))
                {
                    server = new Serverhost(input.Split(' ')[1]);
                    DebugPrint.Print("Server created!");

                    running = true;

                    while (running)
                        server.Update();
                }
                #region delete when loading json is finished
                else if (input.ToLower().StartsWith("loadjson"))
                {
                    JsonStuff.LoadJson();
                }
                #endregion
                else if (input.ToLower().StartsWith("join"))
                {
                    client = new Client(input.Split(' ')[1]);
                    string givenIP = input.Split(' ')[1];

                    DebugPrint.Print(string.Format("Connecting to {0}..", givenIP));

                    client.ConnectIp(givenIP);

                    running = true;

                    Thread t = new Thread(new ThreadStart(UpdateLoop));
                    t.Start();

                    while (running)
                    {
                        if (!client.connected && client.hasconnnected)
                        {
                            DebugPrint.Print("You don't appear to be connected, please enter a IP to connect to..", false);
                            client.ConnectIp(Console.ReadLine());
                        }
                        else
                            client.Sendmsg(string.Format("{0}: {1}", hash, Console.ReadLine()), DataType.DATATYPEMSG);
                    }
                }
                else if (input.StartsWith("config"))
                {
                    DebugPrint.NoFormatWrite(string.Format("Hash [{0}]: ", hash));
                    string usernameToSave = Console.ReadLine();

                    if (usernameToSave == String.Empty)
                        usernameToSave = hash;

                    DebugPrint.NoFormatWrite("Default IP: ");
                    string ipToSave = Console.ReadLine();

                    JsonStuff.ConfigSave(usernameToSave, ipToSave);
                }
                else
                    DebugPrint.Print(string.Format("That command is not on our database, type *help* for help! Given command: {0}", input));
            }
        }

        private static void UpdateLoop()
        {
            while (running)
            {
                if (client.connected)
                    client.Update();
                else
                    client.Update();
            }
            return;
        }
		static bool ConsoleEventCallback(int eventType)
		{
			if (eventType == 2)
			{
				client.Cleanup();
				Console.WriteLine("Console window closing, death imminent");
			}
			return false;
		}

	}
}
