using System;

namespace chattyrecode1
{
    class DebugPrint
    {
		// print to console with warn/error
		public static void Print(string text, bool isFatal)
        {
            if (isFatal)
            {
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine(string.Format("{0} [ERROR]: {1}", DateTime.Now.ToString(), text));

                Console.ForegroundColor = ConsoleColor.Green;
            }
            else
            {
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Yellow;

                Console.WriteLine(string.Format("{0} [WARN]: {1}", DateTime.Now.ToString(), text));

                Console.ForegroundColor = ConsoleColor.Green;
            }
        }

        // print to console with all ok
        public static void Print(string text)
        {
            Console.ResetColor();

            Console.WriteLine(string.Format("{0}: {1}", DateTime.Now.ToString(), text));

            Console.ForegroundColor = ConsoleColor.Green;
        }

        // without any formatting
        public static void NoFormat(string text)
        {
            Console.ResetColor();

            Console.WriteLine(text);

            Console.ForegroundColor = ConsoleColor.Green;
        }
        
        // without any formatting and no newline
        public static void NoFormatWrite(string text)
        {
            Console.ForegroundColor = ConsoleColor.Gray;

            Console.Write(text);

            Console.ForegroundColor = ConsoleColor.Green;
        }
    }
}
